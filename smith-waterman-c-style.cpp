/*
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.
*/

#include <iostream>
#include <string>
using namespace std;

/*
The Smith–Waterman algorithm performs local sequence alignment; that is, for
determining similar regions between two strings of nucleic acid sequences
or protein sequences.

Source:
https://en.wikipedia.org/wiki/Smith–Waterman_algorithm
https://fr.wikipedia.org/wiki/Algorithme_de_Smith-Waterman#Construction_de_l'alignement  (for the alignement construction)
https://genome.tugraz.at/BIOINF/7265238.pdf

Expected result:
https://en.wikipedia.org/wiki/Smith%E2%80%93Waterman_algorithm#Example
*/

int similarity_score(char a, char b){
  if (a == b)
    return 3;
  else
    return -3;
}

void print_matrix(int* M, int nrow, int ncol){
  /// only print matrix M
  for (int i=0; i < nrow; i++){
    for (int j=0; j < ncol; j++){
      printf("%d ",  M[i + j*nrow]);
    }
    printf("\n");
  }
}

void set(int* M, int nrow, int i, int j, int val){
    // M[i,j] = val
    // nrow: rows of M
    M[i + j*nrow] = val;
}

int get(int* M, int nrow, int i, int j){
  // return M[i,j]
  return M[i + j*nrow];
}

int max(int* vec, int len){
  // returns the maximum entry of the vector vec
  // len: length of vec
  int out = vec[0];
  for (int i=1; i<len; i++){
    if (vec[i] > out)
      out = vec[i];
  }
  return out;
}

int* argmax(int* M, int nrow, int ncol){
  // nrow: rows of M
  // ncols: columns of M
  int i_max = 0;
  int j_max = 0;
  int val_max = M[0];

  for (int i=0; i<nrow; i++){
    for (int j=0; j<ncol; j++){
      int val_cur = get(M, nrow, i, j);
      if (val_cur > val_max){
        val_max = val_cur;
        i_max = i;
        j_max = j;
      }
    }
  }
  int* out = new int[3]{i_max, j_max, val_max};
  return out;
}


void align(string c, string r){

  int gap_penalty = 2;

  // initialize the scoring matrix with 0
  int nrow = (r.length()+1);
  int ncol = (c.length()+1);
  int num_el = nrow * ncol;
  int H[num_el];
  int tot = 0;
  for (int i=0; i < nrow; i++){
    for (int j=0; j < ncol; j++){
      set(H, nrow, i, j, 0);
    }
  }

  // fill the scoring matrix - we choose linear gap penalty ( w_k = k*gap_penalty)
  for (int i=1; i<nrow; i++){
    for (int j=1; j<ncol; j++){
      int s = similarity_score(r[i-1], c[j-1]); // we start counting from 1...
      int top_left = get(H, nrow, i-1, j-1) + s;
      int top = get(H, nrow, i-1, j) - gap_penalty;
      int left = get(H, nrow, i, j-1) - gap_penalty;
      int vals[] = {0, top_left, top, left};
      int penalty = max(vals, 4);
      set(H, nrow, i, j, penalty);
    }
  }

  printf("\nscoring matrix H:\n");
  print_matrix(H, nrow, ncol);

  // alignement construction
  int* max = argmax(H, nrow, ncol);
  int i_cur = max[0];
  int j_cur = max[1];
  printf("argmax_ i: %d, j: %d", i_cur, j_cur);
  int top, left, top_left;
  string out_c = "";
  string out_r = "";
  bool next = 1;

  while (next){
    top = get(H, nrow, i_cur-1, j_cur);
    left = get(H, nrow, i_cur, j_cur-1);
    top_left = get(H, nrow, i_cur-1, j_cur-1);

    if (top_left >= top & top_left >=left){
      i_cur--;
      j_cur--;
      out_c = c[j_cur] + out_c; //remind we added on H an empty row and column
      out_r = r[i_cur] + out_r; //remind we added on H an empty row and column
    }else{
      if (top >= left){
        i_cur--;
        out_c = "-" + out_c;
        out_r = r[i_cur] + out_r;
      }else{
        j_cur--;
        out_c = c[j_cur] + out_c;
        out_r = "-" + out_r;
      }
    }
    if (top_left==0 & top==0 & left==0)
      next = 0;
  }

  printf("\n\nInput sequences:\n");
  printf("%s \n", c.c_str());
  printf("%s \n", r.c_str());

  printf("\nOutput aligned sequences:\n");
  printf("%s \n", out_c.c_str());
  printf("%s \n", out_r.c_str());
}

int main(){

  string sequence_1 = "TGTTACGG";
  string sequence_2 = "GGTTGACTA";

  align(sequence_1, sequence_2);

  sequence_1 = "hello ;-) world!";
  sequence_2 = "hellhffho word!";

  align(sequence_1, sequence_2);
}
