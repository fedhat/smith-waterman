The Smith–Waterman algorithm performs local sequence alignment.  


[Algorithm](https://en.wikipedia.org/wiki/Smith–Waterman_algorithm), 
[Paper](https://genome.tugraz.at/BIOINF/7265238.pdf)

## Example with DNA
In biology this algorithm is used to align two sequences of DNA or two protein sequences:
```
Input sequences:
TGTTACGG 
GGTTGACTA 

Output aligned sequences:
G-TT-AC 
GGTTGAC 
```

## Example with natural language 
this algorithm can be used to align two texts:

```
Input sequences:
hello ;-) world! 
hellhffho word! 

Output aligned sequences:
hell----o ;-) world! 
hellhffho ----wor-d!
```

## Compile:  
`g++ smith-waterman.cpp -o smith-waterman.out`

## Execute:  
`./smith-waterman.out `



